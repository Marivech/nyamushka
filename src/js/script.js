const hoverText = "Котэ не одобряет?";



// reset type text when select state is changed
function resetProductType($elt) {
  var $productType = $elt.find('.product-type');
  $productType.text($productType.data("type"));
  $elt.removeClass('hovered');
}



// mark item as selected or undo
function makeItemSelected(e) {
  e.preventDefault();
  const $elt = $(e.currentTarget);
  var $listItem = $elt.closest('.list-item')
  if (!$listItem.hasClass('disabled')) {
    $listItem.toggleClass('selected');
    resetProductType($elt);
  }
};


$(document).ready(function () {
  // change text on selected hover
  $('.product').hover(function() {
      // TODO: remove $(this)
      if ($(this).closest('.list-item').hasClass('selected')) {
        $(this).find('.product-type').text(hoverText);
        $(this).addClass('hovered');
      }
    }, function() {
      // TODO: remove $(this)
      if ($(this).closest('.list-item').hasClass('selected')) {
        $(this).find('.product-type').text($(this).find('.product-type').data("type"));
        $(this).removeClass('hovered');
      }
  });

  // process click on product to select and undo
  $('.product').on('click touch', makeItemSelected);
  // process click on link to select and undo
  $('.buy-product').on('click touch', makeItemSelected);
});
